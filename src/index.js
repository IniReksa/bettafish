import React from 'react';
import ReactDOM from 'react-dom';
import "./assets/css/index.css";
import * as serviceWorker from './serviceWorker';

import {
  BrowserRouter,
  HashRouter,
  Route,
  Switch,
  Redirect
} from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';

import Signup from "./views/Signup";
import Activate from "./views/Activate";
import HomePage from "./views/HomePage";

ReactDOM.render(
  <React.StrictMode>
    <HashRouter>
      <Switch>
        <Route exact path="/home" render={props => <HomePage {...props} />} />
        <Route path="/signup" render={props => <Signup {...props} />} /> 
        <Route path="/signin" render={props => <Signup {...props} />} />
        <Route path="/user/activation/:id" render={props => <Activate {...props} />} />
        <Redirect from="/" to="/home" />
      </Switch>
    </HashRouter>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register();
