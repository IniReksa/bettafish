import React, {Component} from "react";
import axios from "axios";
import {
    Route,
    Switch,
    // Link
  } from "react-router-dom";

import Header from "../components/Header";
import Home from "../components/Home";
import Dashboard from "../components/Dashboard";

class HomePage extends Component{
    constructor(props){
        super(props)
        this.state = {
            isLogin: false
        }
    }

    componentDidMount(){
        if (document.cookie.split(';').some((item) => item.trim().startsWith('token='))) {
            // this.props.history.push('/signin');
            axios
                .get(`${process.env.REACT_APP_API}/me`, {
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODAwMFwvYXBpXC9sb2dpbiIsImlhdCI6MTU4ODk1NTY0MywiZXhwIjoxNTg4OTU5MjQzLCJuYmYiOjE1ODg5NTU2NDMsImp0aSI6IlpsSmpCd0tpSjRkOHVUUTkiLCJzdWIiOjEzLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIiwiaWQiOjEzLCJlbWFpbCI6Im11aGFtYWRhbmdnYTYwOUBnbWFpbC5jb20ifQ.6kzhVEFEMYsLahhYk_NVjAajL6uzFN4NVTKbH7iRXGs'
                    }
                })
        }
    }

    render(){
        return (
            <>
            <Header />
            <Switch>
                <Route exact path={`${this.props.match.path}`} render={props => <Home {...props} />} />
                <Route path={`${this.props.match.path}/dashboard`} render={props => <Dashboard {...props} />} />
            </Switch>
            <div>
                footer
            </div>
            </>
        )
    }
}

export default HomePage