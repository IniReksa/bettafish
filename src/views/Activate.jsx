import React, { Component } from "react";
import axios from "axios";
import Swal from 'sweetalert2'

class Activate extends Component {
    componentDidMount(){
        axios
        .get(`${process.env.REACT_APP_API}/activate/${this.props.match.params.id}`)
        .then(res => {
            if(res.data.error == 0){
                Swal.fire({
                    icon: 'success',
                    title: res.data.message,
                    text: '',
                    showConfirmButton: false,
                    // padding: '10px',
                    timer: 1800,
                    heightAuto: false
                }).then(result => {
                    this.props.history.push("/signin")
                })
            } else {
                Swal.fire({
                    icon: 'error',
                    title: res.data.message,
                    text: '',
                    showConfirmButton: false,
                    // padding: '10px',
                    timer: 1800,
                    heightAuto: false
                }).then(result => {
                    this.props.history.push("/signup")
                })
            }
        })
        .catch(err => {
            Swal.fire({
                icon: 'error',
                title: err.response.data.message,
                text: '',
                showConfirmButton: false,
                // padding: '10px',
                timer: 1800,
                heightAuto: false
            }).then(result => {
                this.props.history.push("/signup")
            })
        })
    }
    render(){
        return(
            <></>
        )
    }
}

export default Activate;