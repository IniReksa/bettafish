import React, {Component} from "react";
import { 
    Container,
    Row, 
    Col
 } from "reactstrap";
// import axios from "axios";

class Home extends Component{
    constructor(props){
        super(props)
        this.state = {}
    }

    render(){
        return (
            <div className="content-home">
                <div 
                    className="banner text-center"
                    style={{
                        backgroundImage: `url('${require("../assets/images/image-banner.png")}')`,
                        backgroundPosition: "center",
                        backgroundRepeat: "no-repeat",
                        backgroundSize: "cover",
                        height: '555px'
                    }}
                >
                    <Row className="align-items-center h-100 justify-content-center">
                        <Col lg="6" md="6">
                            <p className="text-uppercase welcome">Welcome To </p>
                            <p className="text-uppercase welcome">
                                <span style={{
                                    marginRight: "6px", 
                                    color: "#FEA818",
                                    textShadow: "2px 1px 0px white"}}>Betta Fish</span>
                                <span style={{
                                    color: "#164080",
                                    textShadow: "2px 1px 0px white"}}>Marketplace</span>
                            </p>
                            <br/>
                            <p className="welcome-detail">Quis cillum reprehenderit nostrud do in consequat veniam 
                            duis id. Nulla veniam tempor ea dolor occaecat Lorem duis 
                            ad reprehenderit ex enim labore dolore. Irure non esse eu 
                            cillum nostrud sit mollit anim enim veniam sint elit ex.</p>
                        </Col>
                    </Row>
                </div>

                <div className="service">
                    <Container>
                        <Row className="text-center">
                            <Col lg="3" md="3">
                                <div className="service-item">
                                    <div className="bg-white">
                                        <img alt="" src={require(`../assets/images/service_buy.svg`)} />
                                    </div>
                                    <div className="title">Buy & Sell Easly</div>
                                    <p>Lorem Ipsum dolor sit amet Loremipsum dolorsit amet lorem ipsum</p>
                                </div>
                            </Col>
                            <Col lg="3" md="3">
                                <div className="service-item">
                                    <div className="bg-white">
                                        <img alt="" src={require(`../assets/images/service_secure.svg`)} />
                                    </div>
                                    <div className="title">Secure Transaction</div>
                                    <p>Lorem Ipsum dolor sit amet Loremipsum dolorsit amet lorem ipsum</p>
                                </div>
                            </Col>
                            <Col lg="3" md="3">
                                <div className="service-item">
                                    <div className="bg-white">
                                        <img alt="" src={require(`../assets/images/service_control.svg`)} />
                                    </div>
                                    <div className="title">Product Control</div>
                                    <p>Lorem Ipsum dolor sit amet Loremipsum dolorsit amet lorem ipsum</p>
                                </div>
                            </Col>
                            <Col lg="3" md="3">
                                <div className="service-item">
                                    <div className="bg-white">
                                        <img alt="" src={require(`../assets/images/service_quality.svg`)} />
                                    </div>
                                    <div className="title">Quality Platform</div>
                                    <p>Lorem Ipsum dolor sit amet Loremipsum dolorsit amet lorem ipsum</p>
                                </div>
                            </Col>
                        </Row>                
                    </Container>
                </div>
            </div>
        )
    }
}

export default Home