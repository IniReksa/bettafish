import React, { Component } from "react";
import { Link } from "react-router-dom";
import {
    Container,
    Row,
    Button,
    Col,
    Input,
    InputGroup,
    InputGroupAddon 
} from "reactstrap";

class Header extends Component {
    render() {
        return (
            <div className="header">
                <Container>
                    <Row className="justify-content-center">
                        <Col lg="2" md="2">
                            <div className="logo">
                                <Link to="/">
                                    <img className="rounded-circle" src={require(`../assets/images/logo.png`)} alt="" />
                                    Betta Fish
                                </Link>
                            </div>
                        </Col>
                        <Col lg="5" md="5">
                            <div className="advanced-search">
                                <button type="button" className="category-btn">Kategori</button>
                                <InputGroup>
                                    <Input  type="text" placeholder="Cari" />
                                    <InputGroupAddon addonType="append">
                                    <Button color="secondary"><i className="fas fa-search"></i></Button>
                                    </InputGroupAddon>
                                </InputGroup>
                            </div>
                        </Col>
                        <Col lg="5" md="5" className="">
                            <ul className="nav-right">
                                <li className="cart-icon">
                                    <Link to="#">
                                        <img alt="bag" width="25px" src={require(`../assets/images/shopping-bag.svg`)} />
                                        <span>3</span>
                                    </Link>
                                    <div className="cart-hover">
                                        <div className="select-items">
                                            <table>
                                                <tbody>
                                                    <tr>
                                                        <td className="si-pic"><img src="img/select-product-1.jpg" alt="" /></td>
                                                        <td className="si-text">
                                                            <div className="product-selected">
                                                                <p>$60.00 x 1</p>
                                                                <h6>Kabino Bedside Table</h6>
                                                            </div>
                                                        </td>
                                                        <td className="si-close">
                                                            <i className="ti-close"></i>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td className="si-pic"><img src="img/select-product-2.jpg" alt="" /></td>
                                                        <td className="si-text">
                                                            <div className="product-selected">
                                                                <p>$60.00 x 1</p>
                                                                <h6>Kabino Bedside Table</h6>
                                                            </div>
                                                        </td>
                                                        <td className="si-close">
                                                            <i className="ti-close"></i>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div className="select-total">
                                            <span>total:</span>
                                            <h5>$120.00</h5>
                                        </div>
                                        <div className="select-button">
                                            <Link to="#" className="primary-btn view-card">VIEW CARD</Link>
                                            <Link to="#" className="primary-btn checkout-btn">CHECK OUT</Link>
                                        </div>
                                    </div>
                                </li>
                                <li className="heart-icon">
                                    <Link to="#">
                                        <img alt="bag" width="25px" src={require(`../assets/images/notification.svg`)} />
                                        <span>1</span>
                                    </Link>
                                </li>
                                <li className="heart-icon">
                                    <Link to="#">
                                        <img alt="bag" width="25px" src={require(`../assets/images/message.svg`)} />
                                        <span>1</span>
                                    </Link>
                                </li>
                                <li className="heart-icon shop-icon">
                                    <Link to="#">
                                        <img alt="bag" width="30px" src={require(`../assets/images/shop.svg`)} />
                                    </Link>
                                </li>
                                <li className="profile-icon">
                                    <Link to="#">
                                        <img className="rounded-circle" alt="bag" width="35px" src={require(`../assets/images/logo.png`)} />
                                        <span>Name User</span>
                                    </Link>
                                </li>
                            </ul>
                        </Col>
                    </Row>
                </Container>
            </div>
        )
    }
}

export default Header