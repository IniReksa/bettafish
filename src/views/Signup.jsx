import React, { Component } from "react";
import axios from "axios";
import { 
    Row, 
    Col,
    Button,
    Form,
    FormGroup,
    Label,
    Input,
    FormFeedback,
    Spinner
} from "reactstrap";
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'

const MySwal = withReactContent(Swal)

class Signup extends Component {
    constructor(props){
        super(props);
        this.state = {
            username: "",
            email: "",
            password: "",
            confirmPassword: "",
            privacy: false,
            rememberMe: false,
            spinnerSignup: false,
            spinnerSignin: false
        }
    }

    handleChange = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
        if(event.target.name == 'privacy'){
            this.setState({
                privacy: event.target.checked
            })
        }
        if(event.target.name == 'rememberMe'){
            this.setState({
                rememberMe: event.target.checked
            })
        }
    }

    handleSignup = event => {
        event.preventDefault()
        this.setState({
            spinnerSignup: true
        })

        const {
            username,
            email,
            password,
            confirmPassword } = this.state
 
        var form = new FormData();
        form.append("username", username)
        form.append("email", email)
        form.append("password", password)
        form.append("password_confirmation", confirmPassword)

        axios
            .post(`${process.env.REACT_APP_API}/register`, form)
            .then(res => {
                this.setState({
                    spinnerSignup: false
                })
                if(res.data.error == 0){
                    this.customAlert('success', res.data.message, 'Silahkan buka pesan email untuk verifikasi!');
                    this.props.history.push("/signin")
                } else {
                    this.customAlert('error', res.data.message, '');
                }
            })
            .catch(err => {
                this.setState({
                    spinnerSignup: false
                })
                console.log(err)
                this.customAlert('error', err.response.data.message, '');
            })
    }

    handleSignin = event => {
        event.preventDefault()
        this.setState({
            spinnerSignin: true
        })

        const { username, password } = this.state
 
        var form = new FormData();
        form.append("username", username)
        form.append("password", password)

        axios
            .post(`${process.env.REACT_APP_API}/login`, form)
            .then(res => {
                this.setState({
                    spinnerSignin: false
                })
                if(res.data.error == 0){
                    // this.customAlert('success', res.data.message, '');
                    document.cookie = "token=" + res.data.token;
                    this.props.history.push("/home")
                } else {
                    this.customAlert('error', res.data.message, '');
                }
            })
            .catch(err => {
                this.setState({
                    spinnerSignin: false
                })
                if(err.response){
                    this.customAlert('error', err.response.data.message, '');
                }
            })

    }

    customAlert = (icon, title, message) => {
        Swal.fire({
            icon: icon,
            title: title,
            text: message,
            showConfirmButton: false,
            // padding: '10px',
            timer: 1800,
            heightAuto: false
        })
    }

    clearState = () => {
        this.setState({

        })
    }

    render(){
        return(
            <div className="register">
                <img className="image-bg" alt="bg-bettafish" src={require(`../assets/images/785.jpg`)} />
                <div className="layout-register">
                    {
                        this.props.location.pathname == "/signup" ? (
                            <Row className="justify-content-center">
                                <Col xl="5" lg="5" md="5" sm="0">
                                    {/* <Label className="enjoy">Enjoy & <br/> Happy Bidding</Label> */}
                                    <img className="left-image" alt="signup-bettafish" src={require(`../assets/images/image-left-register.png`)} />
                                </Col>
                                <Col xl="7" lg="7" md="7" sm="0" className="form-input">
                                    <h2>Sign up</h2>
                                    <p>With social media</p>
                                    <div className="signup-social-media">
                                        <Button className="signup-google"> <div className="g">G</div> Sign up with Google</Button>
                                        <Button className="signup-fb">
                                            <img width="30" alt="signup-fb-bettafish" src={require(`../assets/images/fb-bettafish.png`)} />
                                        </Button>
                                        <Button className="signup-tw">
                                            <img width="30" alt="signup-fb-bettafish" src={require(`../assets/images/tw-bettafish.png`)} />
                                        </Button>
                                    </div>
                                    <div className="separator">
                                        Or
                                    </div>
                                    <Form onSubmit={this.handleSignup}>
                                        <FormGroup>
                                            <Label>Username</Label>
                                            <Input
                                                onChange={this.handleChange}
                                                autoComplete="off"
                                                autoFocus
                                                type="text"
                                                name="username"
                                                minLength="6"
                                                maxLength="128"
                                                pattern={`[A-Z a-z 0-9 _ ' " / , . ( ) ! @ # $ % & * -]+`}
                                            />
                                        </FormGroup>
                                        <FormGroup>
                                            <Label>Email</Label>
                                            <Input
                                                onChange={this.handleChange}
                                                autoComplete="off"
                                                type="email"
                                                name="email"
                                            />
                                        </FormGroup>
                                        <FormGroup>
                                            <Label>Password</Label>
                                            <Input
                                                onChange={this.handleChange}
                                                autoComplete="off"
                                                type="password"
                                                name="password"
                                                minLength="6"
                                                maxLength="128"
                                            />
                                        </FormGroup>
                                        <FormGroup>
                                            <Label>Confirm Password</Label>
                                            <Input
                                                onChange={this.handleChange}
                                                autoComplete="off"
                                                type="password"
                                                name="confirmPassword"
                                                minLength="6"
                                                maxLength="128"
                                                invalid={this.state.password !== this.state.confirmPassword}
                                            />
                                            <FormFeedback>Password doesn't match</FormFeedback>
                                        </FormGroup>
                                        <FormGroup className="ml-4" style={{marginBottom: '5px'}}>
                                            <Input
                                                onChange={this.handleChange}
                                                id="privacy"
                                                type="checkbox"
                                                name="privacy"
                                            />
                                            <Label className="privacy">Creating means you're okay with out 
                                            <br/> <a href="/">Terms of Service, Privacy policy,</a>  an out default notification Settings.</Label>
                                        </FormGroup>
                                        <FormGroup style={{marginBottom: '5px'}}>
                                            <Button 
                                                type="submit" 
                                                style={{width: '100%'}} 
                                                className="signup-google"
                                                disabled={!this.state.privacy}
                                                >
                                                {
                                                    this.state.spinnerSignup ? 
                                                    <Spinner type="grow" color="light" />
                                                    : 
                                                    "Sign up"
                                                }
                                            </Button>
                                        </FormGroup>
                                        <FormGroup style={{marginBottom: '5px'}}>
                                            <a href="/signin">
                                                <Button type="button" style={{width: '100%'}} className="signup-google">
                                                    Sign in
                                                </Button>
                                            </a>
                                        </FormGroup>
                                        <Label className="mt-5 privacy">This site is protected by reCAPTCHA and the Google 
                                        <br/>Privacy Policy and Terms of Service apply</Label>
                                    </Form>
                                </Col>
                            </Row>
                            ) : (
                            <Row className="justify-content-center">
                                {/* signin layout */}
                                <Col xl="5" lg="5" md="5" sm="0">
                                    <Label className="enjoy">Enjoy & <br/> Happy Bidding</Label>
                                    <img className="left-image-signin" alt="signup-bettafish" src={require(`../assets/images/30379.jpg`)} />
                                </Col>
                                <Col xl="7" lg="7" md="7" sm="0" style={{marginTop: '86px'}} className="form-input">
                                    <h2>Sign in</h2>
                                    <p>With social media</p>
                                    <div className="signup-social-media">
                                        <Button className="signup-google"> <div className="g">G</div> Sign in with Google</Button>
                                        <Button className="signup-fb">
                                            <img width="30" alt="signup-fb-bettafish" src={require(`../assets/images/fb-bettafish.png`)} />
                                        </Button>
                                        <Button className="signup-tw">
                                            <img width="30" alt="signup-fb-bettafish" src={require(`../assets/images/tw-bettafish.png`)} />
                                        </Button>
                                    </div>
                                    <div className="separator">
                                        Or
                                    </div>
                                    <Form onSubmit={this.handleSignin}>
                                        <FormGroup>
                                            <Label>Username</Label>
                                            <Input
                                                onChange={this.handleChange}
                                                autoComplete="off"
                                                autoFocus
                                                type="text"
                                                name="username"
                                                minLength="6"
                                                maxLength="128"
                                                pattern={`[A-Z a-z 0-9 _ ' " / , . ( ) ! @ # $ % & * -]+`}
                                            />
                                        </FormGroup>
                                        <FormGroup>
                                            <Label>Password</Label>
                                            <Input
                                                onChange={this.handleChange}
                                                autoComplete="off"
                                                type="password"
                                                name="password"
                                                minLength="6"
                                                maxLength="128"
                                            />
                                        </FormGroup>
                                        <FormGroup className="ml-4" style={{marginBottom: '5px'}}>
                                            <Input
                                                onChange={this.handleChange}
                                                id="rememberMe"
                                                type="checkbox"
                                                name="rememberMe"
                                            />
                                            <Label className="remember-me">Remember Me</Label>
                                        </FormGroup>
                                        <FormGroup style={{marginBottom: '5px'}}>
                                            <Button 
                                                type="submit" 
                                                style={{width: '100%'}} 
                                                className="signup-google">
                                                {
                                                    this.state.spinnerSignin ? 
                                                    <Spinner type="grow" color="light" />
                                                    : 
                                                    "Sign in"
                                                }
                                            </Button>
                                        </FormGroup>
                                        <FormGroup style={{marginBottom: '5px'}}>
                                            <a href="/signup">
                                                <Button type="button" style={{width: '100%'}} className="signup-google">
                                                    Sign up
                                                </Button>
                                            </a>
                                        </FormGroup>
                                        <Label className="mt-5 privacy">This site is protected by reCAPTCHA and the Google 
                                        <br/>Privacy Policy and Terms of Service apply</Label>
                                    </Form>
                                </Col>
                            </Row>
                            )
                        }
                </div>

                {/* layout mobile */}
                {
                    this.props.location.pathname == "/signup" ? (
                        <div className="layout-register-mobile">
                            <img className="left-image" alt="signup-bettafish" src={require(`../assets/images/image-left-register.png`)} />
                            <div className="justify-content-center form-input">
                                <h2>Sign up</h2>
                                <p>With social media</p>
                                <div className="signup-social-media">
                                    <Button className="signup-google"> <div className="g">G</div> Sign up with Google</Button>
                                    <Button className="signup-fb">
                                        <img width="30" alt="signup-fb-bettafish" src={require(`../assets/images/fb-bettafish.png`)} />
                                    </Button>
                                    <Button className="signup-tw">
                                        <img width="30" alt="signup-fb-bettafish" src={require(`../assets/images/tw-bettafish.png`)} />
                                    </Button>
                                </div>
                                <div className="separator">
                                    Or
                                </div>
                                <Form onSubmit={this.handleSignup}>
                                    <FormGroup>
                                        <Label>Username</Label>
                                        <Input
                                            onChange={this.handleChange}
                                            autoComplete="off"
                                            autoFocus
                                            type="text"
                                            name="username"
                                            minLength="6"
                                            maxLength="128"
                                            pattern={`[A-Z a-z 0-9 _ ' " / , . ( ) ! @ # $ % & * -]+`}
                                        />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label>Email</Label>
                                        <Input
                                            onChange={this.handleChange}
                                            autoComplete="off"
                                            type="email"
                                            name="email"
                                        />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label>Password</Label>
                                        <Input
                                            onChange={this.handleChange}
                                            autoComplete="off"
                                            type="password"
                                            name="password"
                                            minLength="6"
                                                maxLength="128"
                                        />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label>Confirm Password</Label>
                                        <Input
                                            onChange={this.handleChange}
                                            autoComplete="off"
                                            type="password"
                                            name="confirmPassword"
                                            minLength="6"
                                            maxLength="128"
                                                invalid={this.state.password !== this.state.confirmPassword}
                                            />
                                        <FormFeedback>Password doesn't match</FormFeedback>
                                    </FormGroup>
                                    <FormGroup className="ml-4" style={{marginBottom: '5px'}}>
                                        <Input
                                            onChange={this.handleChange}
                                            id="privacy"
                                            type="checkbox"
                                            name="privacy"
                                        />
                                        <Label className="privacy">Creating means you're okay with out 
                                        <br/> <a href="/">Terms of Service, Privacy policy,</a>  an out default notification Settings.</Label>
                                    </FormGroup>
                                    <FormGroup style={{marginBottom: '5px'}}>
                                        <Button 
                                            type="submit" 
                                            style={{width: '100%'}} 
                                            className="signup-google"
                                            disabled={!this.state.privacy}
                                            >
                                            {
                                                this.state.spinnerSignup ? 
                                                <Spinner type="grow" color="light" />
                                                : 
                                                "Sign up"
                                            }
                                        </Button>
                                    </FormGroup>
                                    <FormGroup style={{marginBottom: '5px'}}>
                                        <a href="/signin">
                                            <Button type="button" style={{width: '100%'}} className="signup-google">
                                                Sign in
                                            </Button>
                                        </a>
                                    </FormGroup>
                                    <Label className="mt-5 privacy">This site is protected by reCAPTCHA and the Google 
                                    <br/>Privacy Policy and Terms of Service apply</Label>
                                </Form>
                            </div>
                        </div>
                        ) : (
                        <div className="layout-register-mobile">
                            {/* layout mobile sign in */}
                            <img className="left-image-signin" alt="signup-bettafish" src={require(`../assets/images/30379.jpg`)} />
                            <div className="justify-content-center form-input">
                                <h2>Sign in</h2>
                                <p>With social media</p>
                                <div className="signup-social-media">
                                    <Button className="signup-google"> <div className="g">G</div> Sign in with Google</Button>
                                    <Button className="signup-fb">
                                        <img width="30" alt="signup-fb-bettafish" src={require(`../assets/images/fb-bettafish.png`)} />
                                    </Button>
                                    <Button className="signup-tw">
                                        <img width="30" alt="signup-fb-bettafish" src={require(`../assets/images/tw-bettafish.png`)} />
                                    </Button>
                                </div>
                                <div className="separator">
                                    Or
                                </div>
                                <Form onSubmit={this.handleSignin}>
                                    <FormGroup>
                                        <Label>Username</Label>
                                        <Input
                                            onChange={this.handleChange}
                                            autoComplete="off"
                                            autoFocus
                                            type="text"
                                            name="username"
                                            minLength="6"
                                            maxLength="128"
                                            pattern={`[A-Z a-z 0-9 _ ' " / , . ( ) ! @ # $ % & * -]+`}
                                        />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label>Password</Label>
                                        <Input
                                            onChange={this.handleChange}
                                            autoComplete="off"
                                            type="password"
                                            name="password"
                                            minLength="6"
                                                maxLength="128"
                                        />
                                    </FormGroup>
                                    <FormGroup className="ml-4" style={{marginBottom: '5px'}}>
                                        <Input
                                            onChange={this.handleChange}
                                            id="rememberMe"
                                            type="checkbox"
                                            name="rememberMe"
                                        />
                                        <Label className="remember-me">Remember Me</Label>
                                    </FormGroup>
                                    <FormGroup style={{marginBottom: '5px'}}>
                                        <Button 
                                            type="submit" 
                                            style={{width: '100%'}} 
                                            className="signup-google">
                                            {
                                                this.state.spinnerSignin ? 
                                                <Spinner type="grow" color="light" />
                                                : 
                                                "Sign in"
                                            }
                                        </Button>
                                    </FormGroup>
                                    <FormGroup style={{marginBottom: '5px'}}>
                                        <a href="/signup">
                                            <Button type="button" style={{width: '100%'}} className="signup-google">
                                                Sign up
                                            </Button>
                                        </a>
                                    </FormGroup>
                                    <Label className="mt-5 privacy">This site is protected by reCAPTCHA and the Google 
                                    <br/>Privacy Policy and Terms of Service apply</Label>
                                </Form>
                            </div>
                        </div>
                    )
                }
            </div>
        )
    }
}

export default Signup